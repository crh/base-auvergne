# La sculpture en son lieu : chapiteaux romans d'Auvergne

Ce dépôt contient le code du site « La Sculpture en son lieu : chapiteaux romans d'Auvergne » ([auvergne.huma-num.fr](htttp://auvergne.huma-num.fr)). Il propose des outils et des analyses concernant cinq églises d'Auvergne (Saint-Pierre de Mozat, Saint-Nectaire, Notre-Dame-du-Port à Clermont-Ferrand, Notre-Dame d'Orcival et Saint-Marcellin de Chanteuges) :

* Un Corpus topo-photographique des chapiteaux présente, pour chaque église, un plan permettant de faire apparaître, de localiser et de comparer la totalité des chapiteaux de l'édifice.

* La revue _Iter et Locus. Lieu rituel et agencement du décor sculpté_ propose une étude d'ensemble des chapiteaux des cinq églises mentionnées et une méthodologie spécifique pour analyser l'agencement du décor sculpté, dans ses relations avec la constitution de l'édifice ecclésial.

Le site est réalisé sous la direction scientifique de Jérôme Baschet, Jean-Claude Bonne et Pierre-Olivier Dittmar, par Marjorie Burghart, Rubi Cortes (de la société des Mots & des Mains) et Martin Holmes (IMT). La mise en œuvre du site a été réalisée par Chloé Maillet. Depuis 2020, Jean-Damien Généro (CNRS) assure l'administration du site et de la base de données iconographiques.

## Structure

Le site contient deux grandes parties :

- Le plan des églises est généré à partir de documents XML au format TEI utilisables par [Image Markup Tool](https://tapor.uvic.ca/~mholmes/image_markup).
- La gallerie des miniatures est elle générée avec Albulle.

##  Lancer le site en local

```shell
cd base-auvergne
php -S localhost:8000
```

## Instances

- GitHub EHESS : [lien](https://github.com/CRH-EHESS/base-auvergne) _(non-maintenu depuis novembre 2017)_
- GitLab Huma-Num : [lien](https://gitlab.huma-num.fr/crh/base-auvergne) _(current)_

## Licence

L'ensemble des fichiers et données de ce dépôt est placé sous les termes de la licence ouverte 2.0 ([texte français](https://www.etalab.gouv.fr/wp-content/uploads/2017/04/ETALAB-Licence-Ouverte-v2.0.pdf) | [english text](https://www.etalab.gouv.fr/wp-content/uploads/2018/11/open-licence.pdf))

## Contact

Contact : gestion.sourcesetdonnees [at] ehess [.] fr
