# Installation guide

This guides is intended for deploying the project on a web server (e.g. production or staging).

**Notes:**

* All commands are to be run on the webserver. Use SSH to connect to the server (e.g. `ssh <username>@<host>`).

## Pre-requisite

* PHP7

## Updates

```console
<username>@<host>:/# cd /www/web_main/
<username>@<host>:/www/web_main/ git fetch
...
<username>@<host>:/www/web_main/ git reset --hard origin/main
HEAD is now at <commit_sha1> <commit_msg>
```

## Régénérer les templates HTML produits par Image Markup Tool

En théorie, Image Markup Tool peut importer les fichiers `DATA/plan.xml`, et ré-générer les fichiers `.html` à partir de la feuille de style XML.

En pratique, les fichiers `.xsl` ont été modifié manuellement, pour insérer
le code spécifique d'affichage des miniatures. Il n'est donc plus possible de
régénérer les templates HTML à partir des sources en XML.

Il est en revanche possible d'éditer les fichiers HTML directement.

## Mettre à jour le site

### Téléchargement vers une machine locale

```shell
rsync --progress --recursive auvergne@auvergne.huma-num.fr:/sites/auvergne/www/web_main ./web_main
```

### Envoi vers le serveur distant

```shell
rsync --progress --recursive ./web_main auvergne@auvergne.huma-num.fr:/sites/auvergne/www/web_main
```
