<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>La Sculpture en son lieu : chapiteaux romans d'Auvergne</title>

<style type="text/css">
<!--
body {
	font: 100%/1.4 Verdana, Arial, Helvetica, sans-serif;
	background: #FFF;
	margin: 0;
	padding: 0;
	color: #000;
}

/* ~~ Sélecteurs d'éléments/balises ~~ */
ul, ol, dl { /* En raison des variations entre les navigateurs, il est conseillé d'attribuer une valeur de zéro aux marges intérieures et aux marges des listes. A des fins de cohérence, vous pouvez définir les valeurs désirées dans cette zone ou dans les éléments de liste (LI, DT, DD) qu'elle contient. N'oubliez pas que les paramètres que vous définissez ici se répercuteront sur la liste .nav, sauf si vous rédigez un sélecteur plus spécifique. */
	padding: 0;
	margin: 0;
}
h1, h2, h3, h4, h5, h6, p {
	margin-top: 0;	 /* la suppression de la marge supérieure résout un problème où les marges sortent de leur div conteneur. La marge inférieure restante l'éloignera de tout élément qui suit. */
	padding-right: 15px;
	padding-left: 15px; /* l'ajout de la marge intérieure aux côtés des éléments à l'intérieur des divs, et non aux divs proprement dit, évite le recours à des calculs de modèle de boîte. Une autre méthode consiste à employer une div imbriquée avec marge intérieure latérale. */
}
a img { /* ce sélecteur élimine la bordure bleue par défaut affichée dans certains navigateurs autour d'une image lorsque celle-ci est entourée d'un lien. */
	border: none;
}

/* ~~ La définition du style des liens de votre site doit respecter cet ordre, y compris le groupe de sélecteurs qui créent l'effet de survol. ~~ */
a:link {
	color: #42413C;
	text-decoration: underline; /* à moins que vous ne définissiez un style particulièrement exclusif pour vos liens, mieux vaut prévoir un soulignement, qui garantit une identification visuelle rapide. */
}
a:visited {
	color: #6E6C64;
	text-decoration: underline;
}
a:hover, a:active, a:focus { /* ce groupe de sélecteurs offrira à un navigateur au clavier la même expérience de survol que celle d'une personne employant la souris. */
	text-decoration: none;
}

/* ~~ ce conteneur à largeur fixe entoure toutes les autres divs ~~ */
.container {
	width: 1000px;
	background: #9f774f;
	margin: 0 auto; /* la valeur automatique sur les côtés, associée à la largeur, permet de centrer la mise en page */
	overflow: hidden; /* cette déclaration permet à .container de comprendre où les colonnes flottantes à l'intérieur se terminent et de les contenir */
}

/* ~~ Colonnes pour la mise en page. ~~ 

1) La marge intérieure n'est placée qu'en haut et/ou en bas des divs. Les éléments à l'intérieur de ces divs posséderont une marge intérieure sur les côtés. Vous évitez ainsi de devoir recourir à des « calculs de modèle de boîte ». N'oubliez pas que si vous ajoutez une marge intérieure latérale ou une bordure à la div proprement dite, elle sera ajoutée à la largeur que vous définissez pour créer la largeur totale. Vous pouvez également supprimer la marge intérieure de l'élément dans la div et placer une seconde div à l'intérieur, sans largeur et possédant une marge intérieure appropriée pour votre concept.

2) Toutes les colonnes étant flottantes, aucune marge ne leur a été attribuée. Si vous devez ajouter une marge, évitez de la placer du côté vers lequel vous effectuez le flottement (par exemple, une marge droite sur une div configurée pour flotter vers la droite). Dans de nombreux cas, vous pouvez plutôt employer une marge intérieure. Pour les divs où cette règle ne peut pas être respectée, ajoutez une déclaration « display:inline » à la règle de la div, afin de contourner un bogue qui amène certaines versions d'Internet Explorer à doubler la marge.

3) Comme des classes peuvent être employées à plusieurs reprises dans un document (et que plusieurs classes peuvent aussi être attribuées à un élément), les colonnes ont reçu des noms de classes au lieu d'ID. Par exemple, deux divs de barre latérale peuvent être empilées si nécessaire. Elles peuvent être très facilement remplacées par des ID si vous le souhaitez, pour autant que vous ne les utilisiez qu'une fois par document.

4) Si vous préférez que la navigation se trouve à droite et pas à gauche, faites flotter ces colonnes en sens opposé (toutes vers la droite au lieu de vers la gauche). Leur rendu s'effectuera dans l'ordre inverse. Il n'est pas nécessaire de déplacer les divs dans le code HTML source.

*/
.sidebar1 {
	float: left;
	width: 200px;
	background: #fff;
	padding-bottom: 10px;
	text-align:center;
	letter-spacing: inherit;
	background-position: 255;
	vertical-align: middle;
	margin-top: 20px;
	margin-right: auto;
	margin-bottom: 20px;
	margin-left: auto;
	padding-top: 20px;
}
.content {

	padding: 10px 0;
	width: 600px;
	background: #fff;
	float: left;
		text-align:justify;
	letter-spacing: inherit;
	margin-top: 20px;
	margin-right: auto;
	margin-bottom: 30px;
	margin-left: auto;
}
.sidebar2 {
	float: left;
	width: 200px;
	background: #fff;
	padding-bottom: 10px;
	text-align:center;
	letter-spacing: inherit;
	background-position: 255;
	vertical-align: middle;
	margin-top: 20px;
	margin-right: auto;
	margin-bottom: 20px;
	margin-left: auto;
	padding-top: 20px;
}



/* ~~ classes flottant/effacement diverses ~~ */
.fltrt {  /* cette classe peut servir à faire flotter un élément depuis la droite sur votre page. L'élément flottant doit précéder l'élément à côté duquel il doit apparaître sur la page. */
	float: right;
	margin-left: 8px;
}
.fltlft { /* cette classe peut servir à faire flotter un élément depuis la gauche sur votre page. L'élément flottant doit précéder l'élément à côté duquel il doit apparaître sur la page. */
	float: left;
	margin-right: 8px;
}
.clearfloat { /* cette classe peut être placée sur une div <br /> ou vide, en tant qu'élément final suivant la dernière div flottante (dans le #container) si le paramètre overflow:hidden du .container est supprimé */
	clear:both;
	height:0;
	font-size: 1px;
	line-height: 0px;
}
-->

	

    .titretimes1 {
	font-family: "Times New Roman", Times, serif;
	color: #996633;
	font-size: 40px;
	font-variant: small-caps;
	border-bottom : 2px dotted red ;
}

    .titretimes2 {
	font-family: "Times New Roman", Times, serif;
	color: #b82125;
	font-size: 24px;
	font-variant: small-caps;
}

.contenttimes {
	font-family: "Times New Roman", Times, serif;
	color:#646464;
	font-size: 12pt;
}

p{
	color:#fff;
	line-height:150%;
	font-family: "Times New Roman", Times, serif;
	font-size: 11pt;
	letter-spacing: 0.8pt;
}
    .mentionslegales {
	font-size: 10pt;
	color: #996633;
}
	   a .mentionslegales {
	color: #666;
	text-align: center;
}
    #lienmentionlegale {
	font-family: "Times New Roman", Times, serif;
	color: #666;
	text-align: center;
}

    .mentionslegales a{
	font-size: 10pt;
	color: #996633;
}


    .liengris {
	font-size: 12pt;
	color: #996633;
}
	   a .liengris {
	color: #666;
	text-align: center;
}
    #liengris {
	font-family: "Times New Roman", Times, serif;
	color: #666;
	text-align: center;
}

    .liengris a{
	font-size: 12pt;
	color: #996633;
}



   .apropos {
	font-size: 10pt;
	color: #996633;
}
	   a .apropos {
	color: #666;
	text-align: right;
}
    .apropos {
	font-family: "Times New Roman", Times, serif;
	color: #666;
	text-align: right;
}




    .content ul {
	font-family: "Times New Roman", Times, serif;
	word-spacing: normal;
	list-style-image: url(img/flecheRougetr.png);
	text-align: justify;
	letter-spacing: 0.8pt;
	padding:0 70px 15px 70px;
	color:#fff;
	line-height:150%;
	font-size: 11pt;
}
    .contenttimes strong {
	color: #963;
	
}
.contenttimes a {
	color: #963;
	
}


		#content2{
	background:#ffffff;
	background:rgb(255,255,255);
	width:1000px;
	height:70px;
	text-align:center;
	letter-spacing: inherit;
	background-color: #FFF;
	background-position: 255;
	vertical-align: middle;
	margin-top: 20px;
	margin-right: auto;
	margin-bottom: 20px;
	margin-left: auto;
	padding-top: 20px;
}



    #liensboutons {
	text-align: center;
}
    .boutons {
	font-size: 12px;
	text-align: center;
}

 .titre3 {
	font-size: 12px;
	text-align: center;
	color:#646464;
}




.lDispo {
font-family: "Times New Roman", verdana, sans-serif;
color: #996633 ;
font-size: 12pt ;
margin:0 0px 0px 0px;


}



.lpasDispo {
font-family: "Times New Roman", verdana, sans-serif;
color: #c1b99a ;
font-size: 12pt ;


}

 #footer {
clear: both;
background-image:url('images/footer.jpg');
background-repeat:repeat-x;
height: 25px;
 }


</style></head>

<body>


<?php
//rechercher le lanceur html
include 'footer.php';
?>


<table  width="100%">
  <tr width="100%">

<td width="100%" height="42" align="right">	
	<p class="lpasDispo"><a href="presentation.php">FR</a> <a href="presentation_en.php">EN</a> <a href="presentation_sp.php">SP</a></p>
</td>
</tr>
</table>



<div class="container">
  <div class="sidebar1">
    <p><img src="img/c_gauche.jpg" width="159" height="377"></p>
  <!-- end .sidebar1 --></div>
  
  <div class="content">
  
    <h1 align="center">
        <span class="titretimes1">La Sculpture en son lieu</span>

	    <span class="titretimes2">Chapiteaux romans d'Auvergne</span></h1>

      <p class="titre3">un site conçu et réalisé par</p>

	  <p class="titre3">Jérôme Baschet, Jean-Claude Bonne et Pierre-Olivier Dittmar</p>

      <p class="contenttimes"><strong>Ce site est le fruit de cinq années de travail collectif (2007-2012). Il a été mené au sein du Groupe d'anthropologie historique de l'occident médiéval (GAHOM, intégré en 2016 au groupe AhloMA, « Anthropologie historique du long Moyen Âge »), qui est une équipe du Centre de recherches historiques (Unité mixte de recherche 8558 du Centre national de la recherche scientifique et de l'École des hautes études en sciences sociales). Il a bénéficié d'un financement de l'Agence nationale pour la recherche, dans le cadre de l'enquête «&nbsp;Une culture du Livre dans une société d'illettrés. Parler, figurer, écrire dans l'Europe médiévale&nbsp;», dirigée par Jean-Claude Schmitt.</strong></p>

      <h1><span class="titretimes2">Présentation</span></h1>

    <p class="contenttimes">Le site comporte deux volets distincts, relevant d'une problématique commune visant à comprendre l'œuvre sculptée dans son lieu et à éclairer la distribution générale des chapiteaux au sein de l'espace ecclésial. </p>
    <ul>
<li class="contenttimes"><strong><a href="/sommaire.php" target="_blank">
   Le Corpus topo-photographique des chapiteaux</a></strong> permet de s'interroger sur l'agencement des chapiteaux en les localisant et en les visualisant à leurs emplacements respectifs pour mieux saisir leurs relations. Jusqu'à présent, il n'existait pas d'outil répondant de manière satisfaisante à une telle exigence. Il nous a donc fallu l'élaborer, ce que nous avons fait en adaptant et en intégrant les logiciels libres <a href="http://tapor.uvic.ca/~mholmes/image_markup/" target="_blank"><span class="liengris">Image Markup Tool</span></a> et <a href="http://albulle.jebulle.net/" target="_blank"><span class="liengris">Albulle</span></a>, avec l'aide de Marjorie Burghart, de Rubi Cortes (de la société des Mots & des Mains) et de Martin Holmes (IMT). La mise en œuvre du site a été réalisée par Chloé Maillet. Depuis 2020, Jean-Damien Généro (CNRS) assure l'administration du site et de la base de données iconographiques.</li>

      <li class="contenttimes"> Nos études, regroupées sous le titre «&nbsp;<a href="http://imagesrevues.revues.org/1579" target="_blank"><strong><em>Iter et Locus. </em>Lieu rituel et agencement du décor sculpté dans les églises romanes d'Auvergne</strong></a>&nbsp;», sont accueillies sur le site d'Images Re-vues. Elles reposent essentiellement sur une méthode carto- et info-graphique, recourant à diverses formes de codification thématique et topographique dont l'analyse aide à rendre compte, d'une façon aussi globale que possible, de l'agencement des chapiteaux et de leurs relations dans l'édifice. <br />
D'emblée, nous nous sommes proposé de rendre accessible cette enquête par une publication en ligne, afin que nos études puissent être accompagnées de la présentation intégrale du corpus topo-photographique. L'édition numérique nous a aussi semblé le moyen le plus pertinent et le plus économique de mettre notre travail à la disposition tant des spécialistes que d'un public passionné ou simplement curieux de la sculpture romane.
<br/>&nbsp;
      </li>
    </ul>

      <p class="contenttimes">Ce corpus est accessible à tous : libre à chacun de se l'approprier à sa manière, pour circuler dans l'édifice ou pour produire du matériel pédagogique, en ayant ou non recours à la méthode d'analyse cartographique que nous proposons dans le second volet du site. Les clichés du GAHOM sont téléchargeables et utilisables à des fins non commerciales, conformément à la <a href="https://www.etalab.gouv.fr/licence-ouverte-open-licence" title="Consulter la licence ouverte ETALAB 2" target="_blank"><span class="liengris">Licence Ouverte 2.0</span></a>. Cet outil de visualisation des images est également disponible gratuitement pour traiter d'autres édifices, voire d'autres types d'images.</p>

      <p class="contenttimes">Le code du site est disponible dans un dépôt GitLab : <a href="https://gitlab.huma-num.fr/crh/base-auvergne" target="_blank">base-auvergne</a>.</p>

      <h1><span class="titretimes2">Équipe</span></h1>

      <p class="contenttimes"><strong>Direction scientifique</strong></p>

      <ul>
          <li class="contenttimes" style="font-weight: bold;">Pierre-Olivier Dittmar, maître de conférences (EHESS).</li>
          <li class="contenttimes" style="font-style: italic;">Jérôme Baschet, maître de conférences (EHESS).</li>
          <li class="contenttimes" style="font-style: italic;">Jean-Claude Bonne, directeur d'études (EHESS).</li>
      </ul>

      <br/>

      <p class="contenttimes"><strong>Direction technique</strong></p>

      <ul>
          <li class="contenttimes" style="font-weight: bold;">Jean-Damien Généro, ingénieur d'études (CNRS, depuis 2020).</li>
      </ul>

      <h1><span class="titretimes2">Crédits</span></h1>

      <ul>
          <li class="contenttimes">Direction du projet : Centre de recherches historiques (UMR 8558).</li>
          <li class="contenttimes">Financement : ANR PFEMA (<a href="https://anr.fr/Projet-ANR-06-BLAN-0402" target="_blank">ANR-06-BLAN-0402</a>).</li>
          <li class="contenttimes">Conception et développement du site internet : Chloé Maillet (EHESS), Rubi Cortes (société des Mots & des Mains) et Martin Holmes (Image Markup Tool, IMT).</li>
          <li class="contenttimes">Maintenance du site internet : Marjorie Burghart (EHESS) puis Jean-Damien Généro (CNRS) depuis 2020.</li>
          <li class="contenttimes">Hébergement : TGIR Huma-Num</li>
      </ul>

      <p class="contenttimes" style="font-style: italic;">Contacter l'équipe : <span class="liengris">gestion.sourcesetdonnees [at] ehess [.] fr</span></p>

      <p class="apropos"><br/>
        &nbsp;&nbsp;
        <a href="index.php" alt="Retour à l'accueil">Retour à l'accueil</a> — <a href="termsofservice.php" alt="Mentions légales">Mentions légales</a> — <a href="privacy.php" alt="Politique de confidentialité">Politique de confidentialité</a><br/>
    </p>
           
   
   
   
   
    <!-- end .content --></div>
    
  <div class="sidebar2">
    <p><img src="img/c_droite.jpg" width="159" height="377"></p>
    <!-- end .sidebar2 --></div>
  <!-- end .container --></div>
<div id="content2">
    <table align="center" width="90%">
        <tr>
            <td align="center" width="20%"><a href="https://www.ehess.fr/" target="_blank"><img src="img/ehess-logo.png" width="80" height="auto" alt="EHESS" /></a></td>
            <td align="center" width="20%"><a href="https://www.cnrs.fr/" target="_blank"><img src="img/cnrs-logo.png" width="80" height="auto" alt="CNRS" /></a></td>
            <!--<td align="center" width="16%"><a href="http://ahloma.ehess.fr/" target="_blank"><img src="img/Gahomlogopetit.jpg" width="100" height="50" alt="GAHOM" /></a></td>-->
            <td align="center" width="20%"><a href="https://anr.fr/fr/investissements-davenir/les-investissements-davenir/" target="_blank"><img src="img/investir_avenir.png" width="35%" height="auto" alt="ANR" /></a></td>
            <td align="center" width="20%"><a href="https://www.huma-num.fr/" target="_blank"><img src="img/humanum-logo.png" width="40%" height="auto" alt="Huma-Num" /></a></td>
        </tr>
    </table>
    </span>
</div>
<?php
//rechercher le lanceur html
include 'footer.php';
?>

  
  
</body>
</html>
